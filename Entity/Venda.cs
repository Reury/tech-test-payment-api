using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Context;

namespace tech_test_payment_api.Entity
{
    public class Venda
    {
        [Key]
        public int Id {get;set;}
        public DateTime Data {get;set;}
        public EnumStatusDaVenda Status {get;set;}
        public int VendedorId {get;set;}
        public Vendedor? vendedor {get;set;}
        public List<Produto>? Produtos {get;set;}

    }
}