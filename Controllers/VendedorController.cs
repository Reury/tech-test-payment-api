// using System;
// using System.Collections.Generic;
// using System.Linq;
// using System.Threading.Tasks;
// using Microsoft.AspNetCore.Mvc;
// using tech_test_payment_api.Models;
// using tech_test_payment_api.Context;
// namespace tech_test_payment_api.Controllers



// {
//     [ApiController]
//     [Route("[controller]")]
//     public class VendedorController : ControllerBase
//     {
//         private readonly VendedorContext _context;

//         public VendedorController(VendedorContext context)
//         {
//             _context = context;
//         }

//         [HttpPost("AdicionarVendedor")]
//         public ActionResult Adicionar(Vendedor vendedor)
//         {
//             _context.Add(vendedor);
//             _context.SaveChanges();
            

//             return Ok(vendedor);
        
        
//        }


//         [HttpDelete("RemoverVendedor")]
//         public ActionResult Deletar(int id)
//         {
//             var vendedorBanco = _context.vendedores.Find(id);

//             if(vendedorBanco == null)
//             {
//                 return NotFound();
//             }

//             _context.vendedores.Remove(vendedorBanco);
//             _context.SaveChanges();
//             return NoContent();
//         }

//         [HttpGet("EncontrarVendedorPorCPF")]
//         public IActionResult EncontrarVendedor(string cpf)
//         {
//             // procura um vendedor pelo CPF informado
//             var vendedorBanco = _context.vendedores.Where(x => x.Cpf == cpf);
//             return Ok(vendedorBanco);
//         }

//         [HttpPost("{id}")]
//         public IActionResult Editar(Vendedor vendedor,int id)
//         {

//             var vendedorBanco = _context.vendedores.Find(vendedor.Id);

//             vendedorBanco.Cpf = vendedor.Cpf;
//             vendedorBanco.Nome = vendedor.Nome;
//             vendedorBanco.Telefone = vendedor.Telefone;

//             _context.vendedores.Update(vendedorBanco);
//             _context.SaveChanges();

//             return Ok(vendedor);
//         }

//         [HttpGet("ListaTodosOsVendedores")]
//         public IActionResult ObterTodos()
//         {
//             // TODO: Buscar todas as tarefas no banco utilizando o EF
//             var vendedores = _context.vendedores.ToList();
//             return Ok(vendedores);
//         }

        
//     }
// }