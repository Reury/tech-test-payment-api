using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entity;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _contextVenda;
 

        

        public VendaController(VendaContext contextVenda)
        {
            // _contextProduto = contextProduto;
            _contextVenda = contextVenda;
            
        }
        // [HttpGet("ObterTodasVendas")]
        // public async Task <ActionResult> Listar()
        // {
        //     var vendas = _contextVenda.Vendas.ToList();

        //     return Ok(vendas);
        // }


        [HttpGet("{id}")]
        public async Task <ActionResult> procurarPorId(int id)
        {
            var vendaBanco = _contextVenda.Vendas.Find(id);
            if(vendaBanco== null)
            {
                return NotFound();
            }


            return Ok(vendaBanco);
        }


        [HttpPut("{id}")]
        public async Task <IActionResult> AtualizarVenda(int id, Venda venda)
        {
            var vendaBanco = _contextVenda.Vendas.Find(id);

            if(vendaBanco == null)
            {
                return NotFound();
            }
            // vendaBanco.Produtos = venda.Produtos;
            // vendaBanco.Data = vendaBanco.Data;
            vendaBanco.Status = venda.Status;

            _contextVenda.Vendas.Update(vendaBanco);
            _contextVenda.SaveChanges();
            return Ok();

        }



        [HttpPost("Nova Venda")]
        public async Task <ActionResult> Adicionar(Venda venda)
        {
            if(venda == null){
                return BadRequest(new { Erro = "a venda não pode ser vazia" });
            }
            List<Produto> carrinho = new List<Produto>(){};

            foreach(var item  in venda.Produtos)
            {
                Produto novoProduto = new Produto
                {
                Id=0,
                Nome = item.Nome,
                };
                carrinho.Add(novoProduto);
            }

            Vendedor novoVendedor = new Vendedor{
                Id=0,
                Cpf= venda.vendedor.Cpf,
                Nome = venda.vendedor.Nome,
                Email = venda.vendedor.Email,
                Telefone = venda.vendedor.Telefone,
            };

            var novaVenda = new Venda{
                Id = 0,
                Data = DateTime.UtcNow,
                vendedor = novoVendedor,
                Produtos = carrinho,
                Status = 0
            };


            _contextVenda.Add(novaVenda);
            _contextVenda.SaveChanges();  
            // _contextProduto.Add(carrinho);
            // _contextProduto.SaveChanges();


            var  result = await _contextVenda.SaveChangesAsync();

   
            return Ok(novaVenda);
        
       }


    }
}