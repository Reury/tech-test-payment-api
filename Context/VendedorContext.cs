using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Entity;
using Microsoft.EntityFrameworkCore;


namespace tech_test_payment_api.Context
{
    public class VendedorContext: DbContext
    {
        public VendedorContext(DbContextOptions<VendedorContext> options) : base(options)
        {   
                    


        }
            public DbSet<Vendedor> Vendedores { get; set; }
        // public override int SaveChanges()
        // {
        //     try
        //     {
        //         return base.SaveChanges();

        //     }
        //     catch (exception ex)
        //     {
        //     }
        // }
    }
}